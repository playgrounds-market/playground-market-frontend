# Playgrounds.Market
Playgrounds.Market is a NFT Gaming Platfrom and Arcade
> Front-end for https://playgrounds.market

## Tools:

- Next.js
- Docker

## How to Get Started Developing

```
docker-compose up --build

# or

docker-compose up
```
