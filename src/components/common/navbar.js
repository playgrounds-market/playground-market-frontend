import { connect } from "react-redux";
import { useRouter } from "next/router";
import Button from "./button";
import { truncateAddress } from "../../utility/moiWeb3";
function Navbar({
  navbarContainerStyle = "",
  brandText = "",
  brandTextStyle = "",
  onClick = () => {
    return;
  },
  address,
}) {
  const router = useRouter();
  return (
    <div
      className={`navbar d-flex flex-row justify-content-between align-items-center p-3 ${navbarContainerStyle}`}
    >
      <style jsx>{`
        .navbar {
          height: 4.5rem;
        }
        .brand-text {
          cursor: pointer;
        }
        .nav-btn::hover,
        .btn::focus {
          color: #fff;
          background-color: #474c59 !important;
        }
      `}</style>
      <span
        onClick={() => router.push("/")}
        className={`brand-text ${brandTextStyle}`}
      >
        {brandText}
      </span>
      {address && address.length > 0 ? (
        <p className={`mb-0`}>{truncateAddress(address)}</p>
      ) : (
        <Button
          onPress={() => onClick()}
          buttonStyle={"nav-btn border border-dark text-uppercase"}
        >
          ConnΞct
        </Button>
      )}
    </div>
  );
}
const mapStateToProps = (state) => ({ address: state.session.address });

export default connect(mapStateToProps, {})(Navbar);
