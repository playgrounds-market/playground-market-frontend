import { useState, useEffect } from "react";
import { connect } from "react-redux"; 
import Navbar from "./navbar";
import Sidebar from "./sidebar";
import LoginModal from "../loginModal";
import { init } from "../../actions/web3Actions";

function layout({ children, layoutContainerStyle = "", init }) {
  const [show, setShow] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);
  useEffect(() => {
    setShowSidebar(!window.location.pathname === "/");
  }, []);

  useEffect(() => {
    init();
  }, []);

  return (
    <div className={`d-flex flex-column h-100 w-100  fnt-monospace`}>
      <style jsx global>
        {`
          html,
          body,
          #__next {
            height: 100%;
            width: 100%;
          }
          main > div {
            height: 100%;
          }
          .fnt-monospace {
            font-family: monospace !important;
          }
          .bg-grey {
            background-color: #eee;
          }
          .content-wrapper {
            height: calc(100% - 72px);
            width: 100%;
          }
        `}
      </style>
      <Navbar
        navbarContainerStyle={"sticky-top border border-dark"}
        brandText={`Playgrounds.markΞt`}
        brandTextStyle={`fnt-monospace text-uppercase`}
        onClick={() => setShow(!show)}
      />
      <div className={`content-wrapper contianer-fluid d-flex flex-row`}>
        {/**Side bar */}
        {showSidebar && <Sidebar sidebarContainerStyle={`fnt-monospace`} />}
        {/**Main Content */}
        <main className={`p-2 fnt-monospace w-100   ${layoutContainerStyle}`}>
          {children}
        </main>
      </div>
      {show && <LoginModal handleShow={() => setShow(!show)} />}
    </div>
  );
}
export default connect(null, { init })(layout);
