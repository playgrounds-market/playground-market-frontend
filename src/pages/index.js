/**
 * Home Page
 */
function _index(props) {
  return (
    <div className={`h-100 d-flex flex-column justify-content-center`}>
      <style jsx>
        {`
          .header {
          }
        `}
      </style>
      <div
        className={`header d-flex flex-column align-items-center justify-content-center mb-5`}
      >
        <h1 className="text-uppercase">Playgrounds.MarkΞt</h1>
        <p className="h5">Play. Create. Ξarn.</p>
        <p>
          <i>Coming Soon...</i>
        </p>
      </div>
    </div>
  );
}
export default _index;
