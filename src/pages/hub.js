import Layout from "@/components/common/layout";
/**
 *
 * @param {string} type - used to identify if section is to return Latest,New,Hottest,etc
 * @returns Games based on type of games to return
 */
function GameSection({ type }) {
  const testGames = [
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
    {
      title: "This Is The Best Game Ever",
      descripton:
        "Rember the time when games were good, well thisngs just got better wit this game",
      price: 0,
      tags: [],
    },
  ];
  function Title({ type }) {
    switch (type) {
      case "latest" || "LATEST":
        return <p className="h4">The Latest Mint</p>;

      default:
        return <div></div>;
    }
  }
  function GameCard({ title = "", descripton = "", price = 0, tags = [] }) {
    return (
      <div className="game-card d-flex flex-column mx-2">
        <style jsx>{`
          .game-card {
            width: 200px;
          }
          .game-card-img {
            background-color: #eee;
            height: 150px;
          }
        `}</style>
        <div className="w-100 game-card-img">
          <img className="w-100 h-100" src="" alt="" />
        </div>
        <div className={"d-flex flex-row"}>
          <p>{title}</p>
          <p>
            <span className="bg-grey px-1">{price}</span>
          </p>
        </div>
        <p>{descripton}</p>
        {/* <p>{tags}</p> */}
      </div>
    );
  }
  function GameGallery({ gameArray }) {
    return (
      <div className={`d-flex flex-row flex-wrap`}>
        {gameArray.map(({ title, descripton, price, tags }) => (
          <GameCard
            title={title}
            descripton={descripton}
            price={price}
            tags={tags}
          />
        ))}
      </div>
    );
  }
  return (
    <div className={`${type}-section` + `w-100 px-5`}>
      {/**
       * Uses
       */}
      <Title type={type} />
      <hr />
      <GameGallery gameArray={testGames} />
    </div>
  );
}

/**
 * Home Page
 */
function Hub(props) {
  return (
    <span>
      <GameSection type={"latest"} />
    </span>
  );
}
export default Hub;
