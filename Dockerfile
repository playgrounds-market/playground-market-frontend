FROM node:lts

#Creates Working App
WORKDIR /usr/app
#copy's package.json file and installs deps
COPY package.json ./
#bundles source
COPY . .
# Port App is Running on
EXPOSE 3000